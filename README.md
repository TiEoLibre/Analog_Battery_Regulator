**Welcome on the Open Small Wind Turbine Analog Charge Controller (OSWACC) project.**

**About**

This project is supported by [Ti'Eole](http://www.tieole.fr) small wind company in France as well as [Tripalium](www.tripalium.org) and [Wind Empowerment](www.windempowerment.org), french and international small wind turbines folk networks.

**Version**
The V3.1 is the current stable version with trhoughole components. It has been tested on several windy sites for more than a year and works fine. Extra information about currently installed charge controllers is on its way.

**Contribute**

If you wish to contribute to this project, fork the project, and see the [issues](https://gitlab.com/TiEoLibre/Analog_Battery_Regulator/-/issues) section to help with the ongoing tasks.

**Feedback**

If you have already installed an OSWACC and would like to suggest a modification or say you're happy, please, [open an issue](https://gitlab.com/TiEoLibre/Analog_Battery_Regulator/-/issues)
You can also post it on the "feedback" room of the OSWACC community [forum](https://gitter.im/Analog_charge_controller/). 

**Support** 

You need to troubleshoot something, some piece of advice or so ? We are happy to help, you can ask for help in the "Support" room of the OSWACC community [forum](https://gitter.im/Analog_charge_controller/).

**Getting started**

To get started, please, visit the [Wiki](https://gitlab.com/TiEoLibre/Analog_Battery_Regulator/-/wikis/home) page.

If you read the documentation, everything will be fine. 

**Order a kit**

If you wish to build an OSWACC with a kit, please contact [Ti'Eole](http://www.tieole.fr) to order a components kit.