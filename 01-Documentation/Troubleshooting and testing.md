# Troubleshooting and testing

Analog Charge controller V3.1 

**Power supply**

Connect a variable DC power supply to the battery input of the controller. Rise the voltage to above 18V and check that the output (pin number 3) of the NPN transistor Q1 does not go above 15.5V.



**Visualize PWM output**

Connect the oscilloscope channel to the output of the D4 or D5. Set the trigger between 5 and 10 volts on the corresponding channel to detect the signal. 

Time scale : 

Procedure: increase the input voltage with a stabilized DC power supply til you see the signal getting high level. Then try to stabilize

To increase the topping voltage, turn counterclockwise the potentiometer. 



### Baremetal calibration

1) Grab the output of either D4 or D5 diode with an oscilloscope probe,  and make sure the oscilloscope ground is connected to the battery minus  connector.
2) Connect a DC power supply to the oswacc battery input  connectors and also a DC voltmeter (don't trust dc power supply display)
3) Keep the temperature sensor at the target battery reference temperature (usually at 20°C)
4) Raise DC voltage until you can observe PWM duty cycle on your oscilloscope
5) Try to stabilize the PWM, ideally at 50% duty cycle with by playing with your DC power supply fine voltage tuning
6) Once stabilized, read the voltage on your voltmeter. This is the  voltage at which oswacc will maintain the DC bus in case of excess  energy.
7) If the voltage is higher than required : turn the tuning potentiometer to see the duty cycle increase.
8) Play again with the DC power supply voltage to stabilize the PWM duty  cycle to 50%. You should notice that the DC voltage at which oswacc has  stabilized is lower than the previous one
9) If dumping voltage is  lower than required, execute steps 7) and 8) by replacing "increase" in  7) by "decrease" and "higher" by "lower" in 8)



## Temperature sensor

**Sensor terminals (on the board)**

**Sensor - :** 4.2 to 4.5V

**Sensor + :** = close to 2 times “Sensor –“ approximately

**Sensor test :**

- Supply the sensor (5V between lead + an - )  
- Read voltage between  